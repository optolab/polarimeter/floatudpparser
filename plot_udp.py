import socket
import numpy as np
import os
import ipdb
from signal import signal, SIGTERM
import argparse
import logging
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.style as mplstyle

mplstyle.use('fast')
mpl.rcParams['path.simplify_threshold'] = 1.0

dt = np.dtype(np.float32)
dt = dt.newbyteorder('>')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class DataReader:

    def __init__(self, port=52869, datasets=6):
        self.port = port
        self.n_datasets = datasets

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', self.port))
        logger.info(f'Opening port {port}')

    @staticmethod
    def read_float_data(data):
        return np.frombuffer(data, dtype=dt)

    def receive_data(self):
        logger.info('Waiting for data')
        data, _ = self.sock.recvfrom(2048)
        data = self.read_float_data(data[17:])
        data = data.reshape(-1, self.n_datasets).transpose()
        return data


class DataPlotter:

    def __init__(self, data_reader: DataReader, batch=60, buffer_length=32, dataset_labels=None, decimate=4):
        self.data_reader = data_reader
        self.batch = batch // decimate
        self.decimate = decimate
        self.buff_len = buffer_length * self.batch
        self.dataset_labels = dataset_labels or [f'Dataset {i}' for i in range(self.data_reader.n_datasets)]
        self.n_ds = self.data_reader.n_datasets
        if dataset_labels:
            self.n_ds = len(dataset_labels)
        self.data_buffer = np.zeros((self.batch * buffer_length, self.n_ds))
        self.frame = 0
        self.max_frames = 4
        self.n_data = 0

    def animate_plot(self):
        logger.info('Starting animation')
        fig, axs = plt.subplots(self.n_ds)
        x = np.arange(0, self.buff_len)
        ax_lines = []
        for i, ax in enumerate(axs):
            ax.set_ylim(-1, 2)
            ax.set_xlim(0, self.buff_len)
            ax.set_title(self.dataset_labels[i])
            ax.set_xlabel('Time')
            ax.set_ylabel('Value')
            ax.grid()
            line, = ax.plot(x, self.data_buffer[:, i])
            ax_lines.append((ax, line))

        # plt.ylim(0, 1)

        def update(data):
            self.frame += 1
            self.n_data += 60
            data = data[:self.n_ds, ::self.decimate]
            self.data_buffer = np.roll(self.data_buffer, -self.batch, axis=0)
            self.data_buffer[-self.batch:, :] = data.transpose()

            if self.frame > self.max_frames:
                logger.info(f'Updating plot, data: {self.n_data}')
                self.frame = 0
                for i, (ax, l) in enumerate(ax_lines):
                    data = self.data_buffer[:, i]
                    l.set_ydata(data)
                    ax.set_ylim(np.min(data), np.max(data))

        def data_gen():
            while True:
                #yield self.data_reader.receive_data()
                yield np.random.rand(self.n_ds, 60)

        ani = animation.FuncAnimation(fig, update, data_gen, interval=10)
        plt.show()


if __name__ == '__main__':
    labels = ['S0', 'S1', 'S2', 'S3', 'BAL']
    reader = DataReader()
    plotter = DataPlotter(reader, dataset_labels=labels, decimate=4)
    plotter.animate_plot()
