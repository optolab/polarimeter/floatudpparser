import h5py
import numpy as np
from scipy.signal import spectrogram
import matplotlib.pyplot as plt

FS = 8192
SCALE = 1


def main():
    with h5py.File('2021-01-29_09-28-40.232833_2021-01-29_09-29-46.429821.hdf5') as f:
        ds = f['polarimeter']

        f, t, Sxx = spectrogram(np.array(ds[::SCALE]), int(FS/SCALE))
        cmap = plt.get_cmap('PiYG')
        # cmap = plt.get_cmap('GnBu')

        Sxx = np.log(Sxx)
        plt.pcolormesh(t, f, Sxx, cmap=cmap)
        plt.ylabel('Frequency [Hz]')
        plt.xlabel('Time [sec]')
        # plt.yscale('symlog')
        plt.show()


if __name__ == '__main__':
    main()
