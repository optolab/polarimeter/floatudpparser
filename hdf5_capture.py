import socket
import numpy as np
import os
import ipdb
import h5py
from datetime import datetime
from signal import signal, SIGTERM
import argparse

PORT = 52867
BATCH = 256
MAX_SAMPLES = 150 * 10 ** 6
MAX_DS_SIZE = int(MAX_SAMPLES / BATCH)

dt = np.dtype(np.float32)
dt = dt.newbyteorder('>')


def read_float_data(data):
    return np.frombuffer(data, dtype=dt)


def get_time():
    date = str(datetime.now())
    date = date.replace(':', '-')
    date = date.replace(' ', '_')
    return date


def receive_data(port=PORT, n_datasets=1, tag=''):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', port))
    print(f'Opening port {port}')
    print(f'Opening hdf5 container')
    start = get_time()

    container = h5py.File(tag + start, 'w')
    ds_id = 0
    pkg_size = BATCH // n_datasets
    datasets = []

    try:
        while True:
            datasets = []
            print(f'Creating new dataset with ID:{ds_id}')
            for i in range(n_datasets):
                datasets.append(
                    container.create_dataset(
                        f'{i}-{ds_id}',
                        (256 * BATCH,),
                        dtype='f',
                        maxshape=(None,),
                    )
                )

            now = datetime.now()
            dataset_start = now

            for ds in datasets:
                ds.attrs[f'start'] = str(now)
                ds.attrs[f'epoch_start'] = int(now.timestamp())

            for i in range(MAX_DS_SIZE):
                data, _ = sock.recvfrom(1024 * 1024)
                data = read_float_data(data[17:])
                print(f'Receiving dataset: {ds_id}, {i}th batch')

                for j, ds in enumerate(datasets):
                    ds[i * pkg_size:(i + 1) * pkg_size] = data[j::n_datasets]
                    ds.resize(((i + 2) * pkg_size,))

            now = datetime.now()
            dataset_end = now

            fs = int(
                (MAX_DS_SIZE * BATCH / n_datasets) /
                (dataset_end - dataset_start).total_seconds()
            )

            print(f'Evaluated sampling frequency is {fs}')

            for ds in datasets:
                ds.attrs['end'] = str(now)
                ds.attrs['epoch_end'] = int(now.timestamp())
                ds.attrs['Fs'] = fs

            ds_id += 1

    except KeyboardInterrupt:
        print('Save files and exit.')

    finally:
        now = datetime.now()
        dataset_end = now

        for ds in datasets:
            ds.attrs['end'] = str(now)
            ds.attrs['epoch_end'] = int(now.timestamp())
            fs = int(
                (len(ds)) /
                (dataset_end - dataset_start).total_seconds()
            )
            ds.attrs['Fs'] = fs
            print(f"Evaluated sampling frequency is {ds.attrs['Fs']}")

        print('Closing file and socket')
        sock.close()

        print(f'Closing hdf5 container')
        container.close()

        end = get_time()
        print('Renaming')
        os.rename(tag + start, f'{tag}{start}_{end}_{fs}.hdf5')


def key_interrupt(sig, name):
    if key_interrupt.count > 0:
        pass
    else:
        key_interrupt.count += 1
        raise KeyboardInterrupt


key_interrupt.count = 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, help='Receiving port number', required=True)
    parser.add_argument('-n', '--datasets', type=int, help='Specify number of parallel data streams', required=True)
    parser.add_argument('-t', '--tag', help='Specify prefix of dataset hdf5 file', default='')

    args = parser.parse_args()

    signal(SIGTERM, key_interrupt)

    receive_data(args.port, args.datasets, args.tag)


if __name__ == '__main__':
    main()
