import h5py
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='HDF% file')
    args = parser.parse_args()

    with h5py.File(args.file) as f:
        print(f.keys())
        print(f['polarimeter'])
        print(list(f['polarimeter']))


if __name__ == '__main__':
    main()
