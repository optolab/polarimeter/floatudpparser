import multiprocessing as mp
from hdf5_capture import receive_data, key_interrupt
from signal import signal, SIGTERM


def main():
    print('Running receiving processes in parallel')
    processes = []

    try:
        args_combinations = [
            (52868, 4, '4diody'),
            (52867, 1, 'balanced'),
        ]

        print(f'Processes parameters {args_combinations}')
        for args in args_combinations:
            processes.append(mp.Process(target=receive_data, args=args))
            processes[-1].start()

        for p in processes:
            p.join()

    except KeyboardInterrupt:
        print("Quiting")
        for p in processes:
            p.join()


if __name__ == '__main__':
    signal(SIGTERM, key_interrupt)
    main()
