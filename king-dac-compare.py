import multiprocessing as mp
import sys
import logging
import h5py
import argparse
import ADS1263
from datetime import datetime as dt
from hdf5_capture import receive_data, get_time

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__file__)


class MasterDataSaver:

    def __init__(self, tag, mic_filter=' ', buffer_size=24000):
        self.tag = tag
        self.mic_filter = mic_filter
        self.buffer_size = buffer_size
        self.spi_buffer = 2048
        self.channel = 9
        self.fs = 96000
        self.timestamp = get_time()
        self._procs = []
        self.running = mp.Value('b', True)

    def stop(self):
        self.running.value = False

    def start(self):
        for p in self._procs:
            p.start()

    def join(self):
        for p in self._procs:
            p.join()

    def destroy(self):
        for p in self._procs:
            p.terminate()

    def __call__(self):
        logger.debug('Initializing processes')
        self._procs.append(mp.Process(target=self.store_spi))
        self._procs.append(mp.Process(target=self.store_udp))
        self._procs.append(mp.Process(target=self.store_soundcard))

        try:
            self.start()
            self.join()
        except KeyboardInterrupt:
            logger.warning('Catching keyboard interrupt. Quitting!!!')
            self.stop()
            self.join()
            self.destroy()

    def store_spi(self):
        ADC = ADS1263.ADS1263()
        if (ADC.ADS1263_init_ADC1('ADS1263_38400SPS') == -1):
            raise RuntimeError
        ADC.ADS1263_SetMode(0)
        cont = h5py.File('_'.join([self.tag, self.timestamp, 'waveshare.hdf5']), 'w')
        ds = cont.create_dataset('0', (self.spi_buffer,), dtype='f', maxshape=(None,))
        start = dt.now()
        ds.attrs[f'start'] = str(start)
        ds.attrs[f'epoch_start'] = int(start.timestamp())
        try:
            i = 0
            while self.running:
                ds[i] = ADC.ADS1263_GetChannalValue(self.channel)
                i += 1
                if i % self.spi_buffer == 0:
                    ds.resize((len(ds) + self.spi_buffer,))
        finally:
            end = dt.now()
            ds.attrs['end'] = str(end)
            ds.attrs['epoch_end'] = int(end.timestamp())
            fs = int(
                (len(ds)) /
                (end - start).total_seconds()
            )
            ds.attrs['Fs'] = fs
            cont.close()

    def store_udp(self):
        receive_data(tag=self.tag)

    def store_soundcard(self):
        import soundcard as sp
        microphone = list(filter(lambda x: x.name.find(self.mic_filter) >= 0, sp.all_microphones()))[0]
        cont = h5py.File('_'.join([self.tag, self.timestamp, 'usbaudio.hdf5']), 'w')
        ds = cont.create_dataset('0', (self.buffer_size,), dtype='f', maxshape=(None,))
        start = dt.now()
        ds.attrs['fs'] = self.fs
        ds.attrs[f'start'] = str(start)
        ds.attrs[f'epoch_start'] = int(start.timestamp())
        with microphone.recorder(samplerate=self.fs, channels=1) as mic:
            try:
                i = 0
                while self.running:
                    logger.debug('Reading mic data')
                    data = mic.record(numframes=self.buffer_size)
                    ds[i * self.buffer_size:(i + 1) * self.buffer_size] = data.reshape(-1)
                    i += 1
            finally:
                logger.warning('Closing usbaudio hdf5 file')
                end = dt.now()
                ds.attrs['end'] = str(end)
                ds.attrs['epoch_end'] = int(end.timestamp())
                fs = int(
                    (len(ds)) /
                    (end - start).total_seconds()
                )
                ds.attrs['Fs'] = fs
                cont.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--tag', help='Add file prefix', required=True)

    args = parser.parse_args()
    saver = MasterDataSaver(args.tag)

    logger.info('Running data saver.')
    saver()
    logger.info('quit...')


if __name__ == '__main__':
    sys.exit(main())
