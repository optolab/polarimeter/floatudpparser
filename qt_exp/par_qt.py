import sys
import random
import time
import numpy as np
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtCore import QTimer, QProcess, pyqtSignal, QObject
import multiprocessing as mp


SAMPLE_RATE = 12000  # 12 kHz
BUFFER_SIZE = int(SAMPLE_RATE * 0.05)  # Buffer 50ms worth of data (600 samples)
NUM_SIGNALS = 5  # Number of signals (for subplots)
UPDATE_INTERVAL = 50  # Update every 50ms

# Worker function for multiprocessing
def signal_generator(queue, signal_index):
    while True:
        # Generate BUFFER_SIZE random samples for each signal
        data = [random.uniform(-1, 1) for _ in range(BUFFER_SIZE)]
        queue.put((signal_index, data))
        time.sleep(1 / SAMPLE_RATE)  # Simulate 12 kHz generation delay


class DataUpdate(QObject):
    # Custom signal to update data in the main thread
    data_ready = pyqtSignal(int, list)


class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        # Create 5 subplots (single column, 5 rows)
        self.axes = [fig.add_subplot(5, 1, i + 1) for i in range(5)]
        super(MplCanvas, self).__init__(fig)


class MainWindow(QMainWindow):
    def __init__(self, queues):
        super().__init__()

        # Setting up the main widget and layout
        self.setWindowTitle("Real-Time 1D Signal Plot with 5 Subplots at 12 kHz (Multiprocessing)")
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        layout = QVBoxLayout(self.central_widget)

        # Create a plot canvas and add to the layout
        self.canvas = MplCanvas(self, width=5, height=8, dpi=100)
        layout.addWidget(self.canvas)

        # Label to show the samples per second (sampling rate)
        self.label = QLabel("Samples per second: 0", self)
        layout.addWidget(self.label)

        # Signal parameters for 5 subplots
        self.xdata = [list(range(BUFFER_SIZE)) for _ in range(5)]  # X data for 5 signals
        self.ydata = [[0] * BUFFER_SIZE for _ in range(5)]  # Y data for 5 signals

        # Start time for sample rate calculation
        self.start_time = time.time()
        self.num_samples = 0

        # Queues for receiving data from worker processes
        self.queues = queues

        # Data update signal handler
        self.data_update = DataUpdate()
        self.data_update.data_ready.connect(self.update_data)

        # Set up timer for the real-time updates
        self.timer = QTimer()
        self.timer.setInterval(UPDATE_INTERVAL)  # Update every 50ms
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

        # Sample generation process using multiprocessing
        self.processes = []
        for i in range(NUM_SIGNALS):
            process = mp.Process(target=signal_generator, args=(self.queues[i], i))
            process.start()
            self.processes.append(process)

        # Timer to pull data from queues
        self.queue_timer = QTimer()
        self.queue_timer.setInterval(1)  # Check the queue frequently
        self.queue_timer.timeout.connect(self.check_queues)
        self.queue_timer.start()

    def check_queues(self):
        for i in range(NUM_SIGNALS):
            try:
                # Pull data from the queue (non-blocking)
                signal_index, data = self.queues[i].get_nowait()
                self.data_update.data_ready.emit(signal_index, data)
            except:
                pass  # Queue is empty, do nothing

    def update_data(self, signal_index, data):
        # Update the ydata for the corresponding signal
        self.ydata[signal_index] = data
        self.num_samples += len(data)

    def update_plot(self):
        # Update the plot for each subplot
        for i in range(5):
            self.canvas.axes[i].clear()
            self.canvas.axes[i].plot(self.xdata[i], self.ydata[i], 'b')
            self.canvas.axes[i].set_ylim(-1.5, 1.5)
            self.canvas.axes[i].set_title(f"Signal {i + 1}")
        
        self.canvas.draw()

        # Update the samples per second every second
        elapsed_time = time.time() - self.start_time
        if elapsed_time >= 1.0:
            samples_per_second = self.num_samples / elapsed_time
            self.label.setText(f"Samples per second: {samples_per_second:.2f}")
            self.start_time = time.time()
            self.num_samples = 0


if __name__ == "__main__":
    # Set up queues for inter-process communication
    queues = [mp.Queue() for _ in range(NUM_SIGNALS)]

    # Start the PyQt application
    app = QApplication(sys.argv)
    main = MainWindow(queues)
    main.show()

    sys.exit(app.exec_())

    # Stop all worker processes when the main window closes
    for process in main.processes:
        process.terminate()

