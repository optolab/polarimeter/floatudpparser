import sys
import random
import time
import numpy as np
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtCore import QTimer

SAMPLE_RATE = 12000  # 12 kHz
BUFFER_SIZE = int(SAMPLE_RATE * 0.05)  # Buffer 50ms worth of data (600 samples)

class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.ax = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Setting up the main widget and layout
        self.setWindowTitle("Real-Time 1D Signal Plot at 12 kHz")
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        layout = QVBoxLayout(self.central_widget)

        # Create a plot canvas and add to the layout
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        layout.addWidget(self.canvas)

        # Label to show the samples per second (sampling rate)
        self.label = QLabel("Samples per second: 0", self)
        layout.addWidget(self.label)

        # Signal parameters
        self.xdata = list(range(BUFFER_SIZE))  # Plot for a window of BUFFER_SIZE samples
        self.ydata = [0] * BUFFER_SIZE

        # Start time for sample rate calculation
        self.start_time = time.time()
        self.num_samples = 0

        # Set up timer for the real-time updates
        self.timer = QTimer()
        self.timer.setInterval(50)  # Update every 50ms (20 updates per second)
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

        # Sample generation timer (generating at 12kHz)
        self.sample_timer = QTimer()
        self.sample_timer.setInterval(int(1000 / SAMPLE_RATE))  # Interval for 12 kHz
        self.sample_timer.timeout.connect(self.generate_sample)
        self.sample_timer.start()

    def generate_sample(self):
        # Generate a new random sample
        new_sample = random.uniform(-1, 1)
        self.ydata.append(new_sample)
        self.ydata.pop(0)  # Remove oldest sample

        # Update sample count for sampling rate calculation
        self.num_samples += 1

    def update_plot(self):
        # Update the plot
        self.canvas.ax.clear()
        self.canvas.ax.plot(self.xdata, self.ydata, 'b')
        self.canvas.ax.set_ylim(-1.5, 1.5)
        self.canvas.ax.set_title("Real-Time Signal at 12 kHz")
        self.canvas.draw()

        # Update the samples per second every second
        elapsed_time = time.time() - self.start_time
        if elapsed_time >= 1.0:
            samples_per_second = self.num_samples / elapsed_time
            self.label.setText(f"Samples per second: {samples_per_second:.2f}")
            self.start_time = time.time()
            self.num_samples = 0


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())

