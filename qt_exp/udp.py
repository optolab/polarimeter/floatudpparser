import sys
import time
import socket
import struct
import multiprocessing as mp
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtCore import QTimer, pyqtSignal, QObject

SAMPLE_RATE = 12000  # 12 kHz
BUFFER_SIZE = int(SAMPLE_RATE * 0.05)  # Buffer 50ms worth of data (600 samples)
NUM_SIGNALS = 5  # Number of signals (for subplots)
UPDATE_INTERVAL = 50  # Update every 50ms
UDP_PORT = 52869  # Port for receiving UDP data
BATCH_SIZE = 60  # Number of vectors in each batch
VECTOR_SIZE = 6  # Each vector contains 6 floats
PACKET_SIZE = BATCH_SIZE * VECTOR_SIZE * 4 * 2 # Total size in bytes for each packet (1440 floats * 4 bytes)

# Worker function for multiprocessing
def signal_generator(queues, udp_port):
    # Set up a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("0.0.0.0", udp_port))

    try:

        while True:
            # Receive a batch of 240 vectors (5760 bytes total)
            data, _ = sock.recvfrom(PACKET_SIZE)
            
            # Unpack data as 1440 floats (6 floats per vector, 240 vectors total)
            values = struct.unpack('!360f', data[17:])  # Big-endian format for 1440 floats
            
            # Reshape data into 240 vectors of 6 floats each
            vectors = [values[i:i + VECTOR_SIZE] for i in range(0, len(values), VECTOR_SIZE)]
            
            # Send the entire batch to all queues
            for queue in queues:
                queue.put(vectors)

            # Add a small delay to simulate the sample rate
            # time.sleep(BATCH_SIZE / SAMPLE_RATE)  # Adjust delay for batch
    except Exception as e:
        print(f"Error in signal generator: {e}")
    finally:
        print("Closing socket in signal generator")
        sock.close()  # Ensure the socket is closed


class DataUpdate(QObject):
    # Custom signal to update data in the main thread
    data_ready = pyqtSignal(int, list)


class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        # Create 6 subplots (single column, 6 rows)
        self.axes = [fig.add_subplot(NUM_SIGNALS, 1, i + 1) for i in range(NUM_SIGNALS)]
        super(MplCanvas, self).__init__(fig)


class MainWindow(QMainWindow):
    def __init__(self, queues):
        super().__init__()

        # Setting up the main widget and layout
        self.setWindowTitle("Real-Time 1D Signal Plot with 6 Subplots at 12 kHz (UDP + Multiprocessing)")
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        layout = QVBoxLayout(self.central_widget)

        # Create a plot canvas and add to the layout
        self.canvas = MplCanvas(self, width=5, height=10, dpi=100)
        layout.addWidget(self.canvas)

        # Label to show the samples per second (sampling rate)
        self.label = QLabel("Samples per second: 0", self)
        layout.addWidget(self.label)

        # Signal parameters for 6 subplots
        self.xdata = [list(range(BUFFER_SIZE)) for _ in range(6)]  # X data for 6 signals
        self.ydata = [[0] * BUFFER_SIZE for _ in range(6)]  # Y data for 6 signals

        # Start time for sample rate calculation
        self.start_time = time.time()
        self.num_samples = 0

        # Queues for receiving data from worker processes
        self.queues = queues

        # Data update signal handler
        self.data_update = DataUpdate()
        self.data_update.data_ready.connect(self.update_data)

        # Set up timer for the real-time updates
        self.timer = QTimer()
        self.timer.setInterval(UPDATE_INTERVAL)  # Update every 50ms
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

        # Sample generation process using multiprocessing
        self.process = mp.Process(target=signal_generator, args=(self.queues, UDP_PORT))
        self.process.start()

        # Timer to pull data from queues
        self.queue_timer = QTimer()
        self.queue_timer.setInterval(1)  # Check the queue frequently
        self.queue_timer.timeout.connect(self.check_queues)
        self.queue_timer.start()

    def check_queues(self):
        for i in range(NUM_SIGNALS):
            try:
                # Pull data from the queue (non-blocking)
                vectors = self.queues[i].get_nowait()
                
                # Extract the corresponding signal data for this subplot
                new_data_batch = [vector[i] for vector in vectors]
                self.data_update.data_ready.emit(i, new_data_batch)
            except:
                pass  # Queue is empty, do nothing

    def update_data(self, signal_index, new_data_batch):
        # Append the batch to the ydata for the corresponding signal
        self.ydata[signal_index].extend(new_data_batch)
        # Trim to maintain a fixed buffer size
        self.ydata[signal_index] = self.ydata[signal_index][-BUFFER_SIZE:]
        self.num_samples += len(new_data_batch)

    def update_plot(self):
        # Update the plot for each subplot
        for i in range(NUM_SIGNALS):
            self.canvas.axes[i].clear()
            self.canvas.axes[i].plot(self.xdata[i], self.ydata[i], 'b')
            min_val, max_val = min(self.ydata[i]), max(self.ydata[i])
            self.canvas.axes[i].set_ylim(min_val, max_val)
            self.canvas.axes[i].set_title(f"Signal {i + 1}")
        
        self.canvas.draw()

        # Update the samples per second every second
        elapsed_time = time.time() - self.start_time
        if elapsed_time >= 1.0:
            samples_per_second = self.num_samples / elapsed_time
            self.label.setText(f"Samples per second: {samples_per_second:.2f}")
            self.start_time = time.time()
            self.num_samples = 0


if __name__ == "__main__":
    # Set up queues for inter-process communication
    queues = [mp.Queue() for _ in range(NUM_SIGNALS)]

    # Start the PyQt application
    app = QApplication(sys.argv)
    main = MainWindow(queues)
    main.show()

    sys.exit(app.exec_())

    # Stop the worker process when the main window closes
    main.process.terminate()

