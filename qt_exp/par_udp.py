import sys
import time
import socket
import struct
import multiprocessing as mp
import signal
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtCore import QTimer, pyqtSignal, QObject

SAMPLE_RATE = 24000  # 12 kHz
BUFFER_SIZE = int(SAMPLE_RATE * 0.1)  # Buffer 50ms worth of data (600 samples)
NUM_SIGNALS = 5  # Number of signals (for subplots)
UPDATE_INTERVAL = 100  # Update every 50ms
UDP_PORT = 52869  # Port for receiving UDP data
BATCH_SIZE = 60  # Number of vectors in each batch
VECTOR_SIZE = 6  # Each vector contains 6 floats
PACKET_SIZE = BATCH_SIZE * VECTOR_SIZE * 4  # Total size in bytes for each packet (1440 floats * 4 bytes)

INDEX_LABEL_MAP = {
    0: "Stokes 0",
    1: "Stokes 1",
    2: "Stokes 2",
    3: "Stokes 3",
    4: "Balanced",
}


# UDP listener process
def udp_listener(queues, udp_port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("0.0.0.0", udp_port))

    try:
        while True:
            data, _ = sock.recvfrom(PACKET_SIZE * 2)
            values = struct.unpack('!360f', data[17:])  # Big-endian format for 1440 floats
            vectors = [values[i:i + VECTOR_SIZE] for i in range(0, len(values), VECTOR_SIZE)]

            # Send data to each queue for individual plots
            for i, queue in enumerate(queues):
                queue.put([vector[i] for vector in vectors])

    except Exception as e:
        print(f"Error in UDP listener: {e}")
    finally:
        print("Closing socket in UDP listener")
        sock.close()


class DataUpdate(QObject):
    data_ready = pyqtSignal(list)  # Signal to update data in the main thread


class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.ax = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class PlotWindow(QMainWindow):
    def __init__(self, queue, signal_index):
        super().__init__()
        self.queue = queue
        self.signal_index = signal_index
        self.setWindowTitle(f"Real-Time Signal Plot of {INDEX_LABEL_MAP[signal_index]}")

        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        self.label = QLabel("Samples per second: 0", self)

        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.addWidget(self.label)

        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)

        self.xdata = list(range(BUFFER_SIZE))
        self.ydata = [0] * BUFFER_SIZE
        self.start_time = time.time()
        self.num_samples = 0

        self.data_update = DataUpdate()
        self.data_update.data_ready.connect(self.update_data)

        self.timer = QTimer()
        self.timer.setInterval(UPDATE_INTERVAL)
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

        self.queue_timer = QTimer()
        self.queue_timer.setInterval(1)
        self.queue_timer.timeout.connect(self.check_queue)
        self.queue_timer.start()

    def check_queue(self):
        try:
            new_data_batch = self.queue.get_nowait()
            self.data_update.data_ready.emit(new_data_batch)
        except:
            pass

    def update_data(self, new_data_batch):
        self.ydata.extend(new_data_batch)
        self.ydata = self.ydata[-BUFFER_SIZE:]
        self.num_samples += len(new_data_batch)

    def update_plot(self):
        self.canvas.ax.clear()
        self.canvas.ax.plot(self.xdata, self.ydata, 'b')
        min_val, max_val = min(self.ydata), max(self.ydata)
        self.canvas.ax.set_ylim(min_val, max_val)
        self.canvas.ax.set_title(f"{INDEX_LABEL_MAP[self.signal_index]}")
        self.canvas.draw()

        elapsed_time = time.time() - self.start_time
        if elapsed_time >= 1.0:
            samples_per_second = self.num_samples / elapsed_time
            self.label.setText(f"Samples per second: {samples_per_second:.2f}")
            self.start_time = time.time()
            self.num_samples = 0


def start_plot_window(queue, signal_index):
    app = QApplication(sys.argv)
    window = PlotWindow(queue, signal_index)

    # Retrieve screen width and calculate the starting x-coordinate
    screen_geometry = app.primaryScreen().geometry()
    screen_width = screen_geometry.width()

    # Set window width and calculate position for a single row arrangement
    window_width = screen_width // NUM_SIGNALS
    window.setFixedWidth(window_width)

    # Move each window to its position in a row
    x_position = signal_index * window_width
    window.move(x_position, 0)  # Align at the top of the screen (y=0)

    window.show()
    sys.exit(app.exec_())


def terminate_processes(processes):
    for process in processes:
        process.terminate()
    for process in processes:
        process.join()


if __name__ == "__main__":
    queues = [mp.Queue() for _ in range(NUM_SIGNALS)]

    # Start UDP listener process
    udp_process = mp.Process(target=udp_listener, args=(queues, UDP_PORT))
    udp_process.start()

    # Start 5 processes for individual plotting windows
    plot_processes = []
    for i in range(NUM_SIGNALS):
        process = mp.Process(target=start_plot_window, args=(queues[i], i))
        process.start()
        plot_processes.append(process)


    # Signal handler for graceful shutdown
    def signal_handler(sig, frame):
        print("Interrupt received, terminating processes...")
        terminate_processes([udp_process] + plot_processes)
        sys.exit(0)


    # Register the SIGINT handler
    signal.signal(signal.SIGINT, signal_handler)

    # Wait for processes to complete
    try:
        udp_process.join()
        for process in plot_processes:
            process.join()
    except KeyboardInterrupt:
        signal_handler(None, None)
