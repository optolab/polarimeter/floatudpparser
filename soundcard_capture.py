import sys
import h5py
import argparse
from dataclasses import dataclass
from datetime import datetime as dt
import logging

import soundcard as sc
from soundcard.pulseaudio import _Microphone as Mic

from hdf5_capture import receive_data, get_time

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@dataclass
class SoundCardSaver:
    file_tag: str = 'soundcard'
    ds_tags: list = None
    fs: int = 48000
    buffer_size: int = 12000
    mic_filter: str = ''
    channels: list = None

    _timestamp: str = None
    _running: bool = True
    _datasets: list = None

    def __post_init__(self):
        self._timestamp = get_time()
        self._datasets = []

    def print_microphones(self):
        for m in self.get_microphones():
            print(m)

    def get_microphones(self):
        return [m for m in sc.all_microphones() if self.mic_filter in m.name]

    def capture_signal(self):
        cont = h5py.File('_'.join([self.file_tag, self._timestamp, 'soundcard.hdf5']), 'w')
        microphone = sc.get_microphone(self.mic_filter)
        if self.channels is None:
            self.channels = list(range(microphone.channels))

        if not self.ds_tags:
            self.ds_tags = [str(i) for i in range(len(self.channels))]

        assert len(self.ds_tags) == len(self.channels), 'Specify the same number of tags as channels'
        for t in self.ds_tags:
            ds = cont.create_dataset(t, (self.buffer_size,), maxshape=(None,))
            ds.attrs['fs'] = self.fs
            start = dt.now()
            ds.attrs[f'start'] = str(start)
            ds.attrs[f'epoch_start'] = int(start.timestamp())
            self._datasets.append(ds)

        with microphone.recorder(samplerate=self.fs, channels=self.channels) as mic:
            try:
                i = 0
                while self._running:
                    logger.debug(f'Reading mic data {i} iteration')
                    data = mic.record(numframes=self.buffer_size)
                    for j, ds in enumerate(self._datasets):
                        ds[i * self.buffer_size:(i + 1) * self.buffer_size] = data[:, j]
                        ds.resize((len(ds) + self.buffer_size,))
                    i += 1
            finally:
                logger.warning('Closing hdf5 file')
                end = dt.now()
                for ds in self._datasets:
                    ds.attrs['end'] = str(end)
                    ds.attrs['epoch_end'] = int(end.timestamp())
                cont.close()


def main():
    action_table = {
        'list': SoundCardSaver.print_microphones,
        'capture': SoundCardSaver.capture_signal
    }
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--mic-filter', default='',
                        help='Microphone filter')
    subparsers = parser.add_subparsers(dest='action', required=True)

    list_parser = subparsers.add_parser('list')

    capture_parser = subparsers.add_parser('capture')
    capture_parser.add_argument('-s', '--sampling-rate', type=int, dest='fs', default=44100)
    capture_parser.add_argument('-f', '--file-tag', default='soundcard')
    capture_parser.add_argument('-t', '--ds-tags', action='append')
    capture_parser.add_argument('-c', '--channels', action='append', type=int)

    args = vars(parser.parse_args())
    func = action_table[args.pop('action')]
    saver = SoundCardSaver(**args)

    func(saver)

    return 0


if __name__ == '__main__':
    sys.exit(main())
