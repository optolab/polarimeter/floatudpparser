import argparse
import h5py
import ipdb
import numpy as np
import matplotlib.pyplot as plt

LABEL_MAP = {
    0: 'Qx',
    1: 'Ix',
    2: 'Qy',
    3: 'Iy',
    4: 'S0',
    5: 'S1',
    6: 'S2',
    7: 'S3',
}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='HDF5 file')

    args = parser.parse_args()

    with h5py.File(args.file) as f:
        for i in range(len(f) // 8):
            fig, (ax1, ax2) = plt.subplots(2, 1)
            for j in range(4):
                ds = f[f'{j}-{i}']
                ax1.plot(np.linspace(0, len(ds) - 1, len(ds)), ds[:], label=LABEL_MAP[j])
            for j in range(4, 8):
                ds = f[f'{j}-{i}']
                ax2.plot(np.linspace(0, len(ds) - 1, len(ds)), ds[:], label=LABEL_MAP[j])
            ax1.legend()
            ax2.legend()
            plt.show()


if __name__ == '__main__':
    main()
