import socket
import numpy as np
import os
import h5py
from datetime import datetime
from signal import signal, SIGTERM
import argparse
import ADS1263

PORT = 52867
BATCH = 2048
MAX_DS_SAMPLES = 30 * BATCH * 60  # approx. 1 hour


def get_time():
    date = str(datetime.now())
    date = date.replace(':', '-')
    date = date.replace(' ', '_')
    return date


def receive_data(channel=9, tag=''):
    start = get_time()
    container = h5py.File(tag + start, 'w')
    ds_id = 0
    datasets = []

    ADC = ADS1263.ADS1263()
    if (ADC.ADS1263_init_ADC1('ADS1263_38400SPS') == -1):
        raise RuntimeError
    ADC.ADS1263_SetMode(0)

    try:
        while True:
            print(f'Creating new dataset with ID:{ds_id}')
            ds = container.create_dataset(
                f'{ds_id}',
                (BATCH,),
                dtype='f',
                maxshape=(None,),
            )

            now = datetime.now()
            dataset_start = now

            ds.attrs[f'start'] = str(now)
            ds.attrs[f'epoch_start'] = int(now.timestamp())

            for j in range(int(MAX_DS_SAMPLES / BATCH)):
                for i in range(BATCH):
                    ds[i] = ADC.ADS1263_GetChannalValue(channel)
                ds.resize((len(ds) + BATCH,))

            now = datetime.now()
            dataset_end = now

            fs = int(
                len(ds) /
                (dataset_end - dataset_start).total_seconds()
            )

            print(f'Evaluated sampling frequency is {fs}')

            ds.attrs['end'] = str(now)
            ds.attrs['epoch_end'] = int(now.timestamp())
            ds.attrs['Fs'] = fs

            ds_id += 1

    except KeyboardInterrupt:
        print('Save files and exit.')

    finally:
        now = datetime.now()
        dataset_end = now

        ds.attrs['end'] = str(now)
        ds.attrs['epoch_end'] = int(now.timestamp())
        fs = int(
            (len(ds)) /
            (dataset_end - dataset_start).total_seconds()
        )
        ds.attrs['Fs'] = fs
        print(f"Evaluated sampling frequency is {ds.attrs['Fs']}")

        print(f'Closing hdf5 container')
        container.close()

        end = get_time()
        print('Renaming')
        os.rename(tag + start, f'{tag}{start}_{end}_{fs}.hdf5')


def key_interrupt(sig, name):
    if key_interrupt.count > 0:
        pass
    else:
        key_interrupt.count += 1
        raise KeyboardInterrupt


key_interrupt.count = 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--channel', help='Specify channel', default=9, type=int)
    parser.add_argument('-t', '--tag', help='Specify prefix of dataset hdf5 file', default='')

    args = parser.parse_args()

    signal(SIGTERM, key_interrupt)

    receive_data(args.channel, args.tag)


if __name__ == '__main__':
    main()
