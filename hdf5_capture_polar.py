import json
import socket
import numpy as np
import os
import h5py
from time import sleep
from datetime import datetime
from signal import signal, SIGTERM
import argparse
from itertools import count
from threading import Thread, Event
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

PORT = 52869
ARM_PORT = 52870
STATUS_PORT = 52871
SOCKET_TIMEOUT = 2.0
BATCH = 256
MAX_SAMPLES = 150 * 10 ** 6
MAX_DS_SIZE = int(MAX_SAMPLES / BATCH)
PKG_SIZE = 60

dt = np.dtype(np.float32)
dt = dt.newbyteorder('>')

POLAR_LABELS = ['S0', 'S1', 'S2', 'S3', 'Bal', 'Label']
ARM_LABELS = ['A1', 'A2']
STATUS_LABELS = {'TYP': h5py.string_dtype(), 'VALUE': float, 'TIMESTAMP': int, 'COUNTER': int}

GLOBAL_STOP = Event()


def read_float_data(data):
    return np.frombuffer(data, dtype=dt)


def get_time():
    date = str(datetime.now())
    date = date.replace(':', '-')
    date = date.replace(' ', '_')
    return date


def receive_data(tag=''):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(SOCKET_TIMEOUT)
    sock.bind(('', PORT))
    logger.info(f'Opening port {PORT}')

    arm_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    arm_sock.settimeout(SOCKET_TIMEOUT)
    arm_sock.bind(('', ARM_PORT))
    logger.info(f'Opening arm port {ARM_PORT}')

    status_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    status_sock.settimeout(SOCKET_TIMEOUT)
    status_sock.bind(('', STATUS_PORT))
    logger.info(f'Opening status port {STATUS_PORT}')

    logger.info(f'Opening hdf5 container')
    start = get_time()

    container = h5py.File(tag + start, 'w')
    pkg_size = PKG_SIZE
    datasets = []

    try:
        datasets = []
        logger.info(f'Creating new dataset for {POLAR_LABELS}')
        for label in POLAR_LABELS:
            datasets.append(
                container.create_dataset(
                    label,
                    (256 * BATCH,),
                    dtype='f',
                    maxshape=(None,),
                )
            )

        arm_datasets = []
        logger.info(f'Creating new dataset for {ARM_LABELS}')
        for label in ARM_LABELS:
            arm_datasets.append(
                container.create_dataset(
                    label,
                    (256 * BATCH,),
                    dtype='f',
                    maxshape=(None,),
                )
            )

        status_dataset = {}
        logger.info(f'Creating new dataset for {STATUS_LABELS}')
        for label, type in STATUS_LABELS.items():
            status_dataset[label] = container.create_dataset(
                label,
                (0,),
                dtype=type,
                maxshape=(None,),
            )

        now = datetime.now()
        dataset_start = now

        # init data containers
        for ds in datasets + arm_datasets:
            ds.attrs[f'start'] = str(now)
            ds.attrs[f'epoch_start'] = int(now.timestamp())

        def receive_polar_data():
            for i in count():  # infinite loop
                if GLOBAL_STOP.is_set():
                    break
                try:
                    data, _ = sock.recvfrom(1024 * 1024)
                except socket.timeout:
                    logger.debug('Socket for polar data has timeout.')
                    continue

                data = read_float_data(data[17:])
                logger.debug(f'Receiving dataset: {i}th batch')

                for j, ds in enumerate(datasets):
                    ds[i * pkg_size:(i + 1) * pkg_size] = data[j::len(POLAR_LABELS)]
                    ds.resize(((i + 2) * pkg_size,))

        def receive_arm_data():
            for i in count():
                if GLOBAL_STOP.is_set():
                    break
                try:
                    data, _ = arm_sock.recvfrom(1024 * 1024)
                except socket.timeout:
                    logger.debug('Socket for arm data has timeout.')
                    continue

                data = read_float_data(data)
                logger.debug(f'Receiving dataset: {i}th batch')

                for j, ds in enumerate(arm_datasets):
                    ds[i * pkg_size:(i + 1) * pkg_size] = data[j::len(ARM_LABELS)]
                    ds.resize(((i + 2) * pkg_size,))

        def receive_status_msgs():
            while not GLOBAL_STOP.is_set():
                try:
                    data, _ = status_sock.recvfrom(1024 * 1024)
                except socket.timeout:
                    logger.debug('Socket for status logs has timeout.')
                    continue
                    
                data = json.loads(data.decode())
                logger.debug(f"Received status message: {data}")

                for label in STATUS_LABELS.keys():
                    status_dataset[label].resize((status_dataset[label].shape[0] + 1,))
                    status_dataset[label][-1] = data[label]

                if data['TYP'] == "STOP":
                    logger.warning("Received stop message! Stopping data collection.")
                    GLOBAL_STOP.set()

        logger.info('Creating threads...')
        # run the following functions in parallel
        polar_thread = Thread(target=receive_polar_data)
        arm_thread = Thread(target=receive_arm_data)
        status_socket_thread = Thread(target=receive_status_msgs)

        logger.info('Starting threads...')
        polar_thread.start()
        arm_thread.start()
        status_socket_thread.start()

        logger.info('Waiting for threads to finish...')
        polar_thread.join()
        arm_thread.join()
        status_socket_thread.join()

    except KeyboardInterrupt:
        logger.warning('Keyboard interput! Save files and exit!')
        key_interrupt(None, None)
        sleep(SOCKET_TIMEOUT)

    finally:
        now = datetime.now()
        dataset_end = now

        for ds in datasets + arm_datasets:
            ds.attrs['end'] = str(now)
            ds.attrs['epoch_end'] = int(now.timestamp())
            fs = int(
                (len(ds)) /
                (dataset_end - dataset_start).total_seconds()
            )
            ds.attrs['Fs'] = fs
            logger.info(f"Evaluated sampling frequency is for {ds.name[1:]}\t->\t{ds.attrs['Fs']}")

        logger.info('Closing file and socket')
        sock.close()

        logger.info(f'Closing hdf5 container')
        container.close()

        end = get_time()
        logger.info(f'Renaming file to {tag}{start}_{end}_{fs}.hdf5')
        os.rename(tag + start, f'{tag}{start}_{end}_{fs}.hdf5')


def key_interrupt(sig, name):
    if key_interrupt.count > 0:
        pass
    else:
        logger.warning('Setting the GLOBAL_STOP')
        key_interrupt.count += 1
        GLOBAL_STOP.set()


key_interrupt.count = 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--tag', help='Specify prefix of dataset hdf5 file', default='')
    parser.add_argument('-l', '--log', help='Specify log level', type=str.upper, default=logging.INFO,
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])

    args = parser.parse_args()

    signal(SIGTERM, key_interrupt)
    logger.setLevel(args.log)
    try:
        import coloredlogs
        coloredlogs.install(level=args.log)
    except ImportError:
        pass

    # Executing the main function
    receive_data(args.tag)


if __name__ == '__main__':
    main()
