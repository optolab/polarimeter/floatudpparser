import struct
import socket
import os
from datetime import datetime

PORT = 52867
SPACER = '\n,'


def process_frame(frame):
    frame = frame[17:]

    parsed_data = []
    for i in range(0, len(frame), 4):
        num = frame[i: i + 4]
        num = num[::-1]
        parsed_data.append(
            struct.unpack('f', num)[0]
        )

    return parsed_data


def get_time():
    date = str(datetime.now())
    date = date.replace(':', '-')
    date = date.replace(' ', '_')
    return date


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', PORT))
    print(f'Opening port {PORT}')

    start = get_time()
    file = open(str(start), 'w')
    print(f'Opening file')
    try:
        print('Starting never ending loop')
        while True:
            print('Receiving')
            data, addr = sock.recvfrom(16 * 1024)
            print('Data received')
            data = process_frame(data)
            file.write(SPACER.join(data))
            file.write(SPACER)

    except KeyboardInterrupt:
        print('Save files and exit.')

    finally:
        print('Closing file and socket')
        file.close()
        sock.close()
        end = get_time()
        print('Renaming')
        os.rename(start, f'{start}_{end}.csv')


if __name__ == '__main__':
    main()
